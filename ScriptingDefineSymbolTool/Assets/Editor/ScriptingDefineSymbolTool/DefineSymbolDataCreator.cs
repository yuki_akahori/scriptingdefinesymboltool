﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace DefineSymbol
{

    // ' is never assigned to, and will always have its default value `null' のコンパイラ警告抑制
#pragma warning disable 0649
    /// <summary>
    /// DefineSymbolDataをJSONファイルから作成する
    /// </summary>
    public static class SymbolDataCreator
    {
        /// <summary>
        /// JSONファイル読み取り用プラットフォーム別シンボル
        /// </summary>
        [System.Serializable]
        class VariantSymbolsForJSON
        {
            public string name;
            public string[] symbols;
        }

        /// <summary>
        /// JSON向けプリセットグループオブジェクト
        /// JSONファイルに記述されたプリセットグループを読み込むためのクラスです
        /// </summary>
        [System.Serializable]
        class PresetGroupForJSON
        {
            public string name;
            public string[] symbols;
            public string[] includePresets;
            public VariantSymbolsForJSON[] variants;
            public bool hidden;
            public string comment;
        }

        /// <summary>
        /// プリセット一覧
        /// </summary>
        [System.Serializable]
        class PresetList
        {
            public PresetGroupForJSON[] presetGroups;
        }

        /// <summary>
        /// 解析処理用プラットフォーム別シンボル
        /// </summary>
        class VariantSymbolsForParse
        {
            public VariantSymbolsForParse(VariantSymbolsForJSON srcVariantSymbosls)
            {
                name = srcVariantSymbosls.name;
                symbols = new List<string>(srcVariantSymbosls.symbols);
            }

            /// <summary>
            /// VariantSymbolsForParseからコピーを作る
            /// シンボル情報が変わる可能性があるのでListを別オブジェクトとして作るようにしている。
            /// stringは変更されないので元の参照をそのまま持つ。
            /// </summary>
            /// <param name="src">コピー元</param>
            public VariantSymbolsForParse(VariantSymbolsForParse src)
            {
                name = src.name;
                symbols = new List<string>(src.symbols.Count);
                foreach (var srcSymbol in src.symbols)
                {
                    symbols.Add(srcSymbol);
                }
            }

            public string name { get; private set; }
            public List<string> symbols;
        }

        /// <summary>
        /// 解析用プリセットグループオブジェクト
        /// 解析処理がやりやすいデータ構造になっています
        /// </summary>
        class PresetGroupForParse
        {
            /// <summary>
            /// コンストラクタ
            /// JSONから作成したPresetForJSONを受け取り、解析用プリセットを作成します
            /// </summary>
            /// <param name="srcPrest">元となるプリセット</param>
            public PresetGroupForParse(PresetGroupForJSON srcPrest)
            {
                name = srcPrest.name;
                // presetの組み合わせだけで、シンボルが定義されないpresetが合った場合、symbolsがnullとなっている。
                symbols = new List<string>(srcPrest.symbols ?? Enumerable.Empty<string>());
                hidden = srcPrest.hidden;
                comment = srcPrest.comment ?? "";   // コメントが設定されていない場合は空文字をセット

                // includePresetsとvariantsは存在しない可能性もあるので、存在しない場合は空を割り当てる
                if (srcPrest.includePresets == null)
                {
                    includePresets = new string[0];
                }
                else
                {
                    includePresets = srcPrest.includePresets;
                }

                if (srcPrest.variants == null || srcPrest.variants.Length == 0)
                {
                    variants = new Dictionary<string, VariantSymbolsForParse>();
                }
                else
                {
                    variants = new Dictionary<string, VariantSymbolsForParse>(capacity: srcPrest.variants.Length);
                    for (var i = 0; i < srcPrest.variants.Length; ++i)
                    {
                        var platform = new VariantSymbolsForParse(srcPrest.variants[i]);
                        variants.Add(platform.name, platform);
                    }
                }
            }

            public string name { get; private set; }
            public string comment { get; private set; }
            public List<string> symbols { get; private set; }
            public string[] includePresets { get; private set; }
            public Dictionary<string, VariantSymbolsForParse> variants { get; private set; }
            public bool hidden { get; private set; }

            /// <summary>
            /// プリセット依存関係の解決
            /// includeを使用している場合、include対象プリセットのデータを含めます。
            /// </summary>
            /// <param name="presetDict"></param>
            public void ResolveDependencies(Dictionary<string, PresetGroupForParse> presetDict)
            {
                if (_resolved)
                {
                    return;
                }

                if (_resolving)
                {
                    throw new System.Exception(string.Format("プリセット {0} で循環参照が発生しています", name));
                }

                _resolving = true;

                // includeしているプリセットの情報を加える
                foreach (var includePreset in includePresets)
                {
                    // include対象のプリセットを取得し、依存関係を解決させる。
                    var addPreset = presetDict[includePreset];
                    addPreset.ResolveDependencies(presetDict);

                    // シンボルを追加
                    symbols.AddRange(addPreset.symbols);

                    // プラットフォーム別シンボルを追加
                    foreach (var addVariant in addPreset.variants.Values)
                    {
                        if (!variants.ContainsKey(addVariant.name))
                        {
                            variants.Add(addVariant.name, new VariantSymbolsForParse(addVariant));
                        }

                        variants[addVariant.name].symbols.AddRange(addVariant.symbols);
                    }
                }

                // includeするプリセットでも同じシンボルが定義されている等、シンボルが重複している可能性もあるので重複を取り除く
                symbols = new List<string>(symbols.Distinct());
                foreach (var platform in variants.Values)
                {
                    platform.symbols = new List<string>(platform.symbols.Distinct());
                }

                _resolved = true;
                _resolving = false;
            }

            /// <summary>
            /// DefineSymbolDataのPresetGroupに変換する
            /// </summary>
            public SymbolData.PresetGroup ToPresetGroup()
            {
                if (!_resolved)
                {
                    throw new System.Exception(string.Format("依存関係を解決していないプリセットです {0} ", name));
                }

                List<SymbolData.Preset> preset;
                if (variants.Count == 0)
                {
                    // プラットフォーム別情報がない場合、Allというプラットフォーム名で一つだけプリセット出力
                    preset = new List<SymbolData.Preset>(capacity: 1);
                    preset.Add(new SymbolData.Preset(name, symbols.ToArray(), comment, variantName: null));
                }
                else
                {
                    // プラットフォーム別にプリセット出力
                    preset = new List<SymbolData.Preset>(capacity: variants.Count);
                    foreach (var platformInfo in variants.Values)
                    {
                        var platformSymbols = symbols.Union(platformInfo.symbols).ToArray();
                        preset.Add(new SymbolData.Preset(name, platformSymbols, comment, platformInfo.name));
                    }
                }

                return new SymbolData.PresetGroup(name, preset.ToArray());
            }

            // 依存関係解決処理用フラグ
            bool _resolved;
            bool _resolving;
        }

        /// <summary>
        /// 設定
        /// </summary>
        [System.Serializable]
        public class Config
        {
            /// <summary>
            /// プリセット一覧ファイルパス
            /// </summary>
            public string[] presetListFilePath;

            /// <summary>
            /// シンボル情報
            /// 設定ツールにシンボルの概要表示に使用します。
            /// 使用する全シンボルに対してこの情報は必要ありません。
            /// シンボルに対応する情報がない場合、何も表示されないだけです。
            /// </summary>
            public SymbolData.SymbolInfo[] symbolInfos;
        }

        /// <summary>
        /// 指定した設定JSONファイルからDefineSymbolDataを作成します
        /// </summary>
        /// <param name="configPath">JSONファイルパス</param>
        public static SymbolData Create(string configPath)
        {
            return Create(LoadFromJson<Config>(configPath));
        }

        /// <summary>
        /// 指定した設定でDefineSymbolDataを作成します
        /// </summary>
        /// <param name="config">設定</param>
        public static SymbolData Create(Config config)
        {
            var presetGroupDict = new Dictionary<string, PresetGroupForParse>();
            for (var i = 0; i < config.presetListFilePath.Length; ++i)
            {
                var presetList = LoadFromJson<PresetList>(config.presetListFilePath[i]);
                foreach (var presetGroup in presetList.presetGroups)
                {
                    presetGroupDict.Add(presetGroup.name, new PresetGroupForParse(presetGroup));
                }
            }

            // 登録したプリセットの依存関係を解決する
            foreach (var preset in presetGroupDict.Values)
            {
                preset.ResolveDependencies(presetGroupDict);
            }

            // DefineSymbolDataを作成する
            var presetGroups = new List<SymbolData.PresetGroup>();
            foreach (var preset in presetGroupDict.Values)
            {
                // hiddenフラグがtrueであればデータを登録しない
                // hiddenはデータを作成するために使用されるプリセットにつかう
                if (!preset.hidden)
                {
                    presetGroups.Add(preset.ToPresetGroup());
                }
            }

            return new SymbolData(config.symbolInfos, presetGroups.ToArray());
        }

        /// <summary>
        /// 指定したJSONファイルからオブジェクトを作成する
        /// </summary>
        static T LoadFromJson<T>(string path)
        {
            var asset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
            if (asset == null)
            {
                throw new System.Exception("テキストアセットが読み込めませんでした: " + path);
            }
            return JsonUtility.FromJson<T>(asset.ToString());
        }
    }

    // #pragma warning disable 0649
}