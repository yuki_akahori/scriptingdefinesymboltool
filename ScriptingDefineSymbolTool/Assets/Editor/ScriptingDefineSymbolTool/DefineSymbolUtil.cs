﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace DefineSymbol
{
    /// <summary>
    /// 定数ユーティリティ
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// 定数シンボル設定ツールの起動
        /// ・設定ファイルのパスからDefineSymbolDataを作る
        /// ・定数設定ツールを起動します。
        /// </summary>
        /// <param name="configPath">設置ファイルパス</param>
        public static void BootConfigTool(string configPath)
        {
            var data = SymbolDataCreator.Create(configPath);
            var presetNames = data.presets
                                .Select(preset => preset.nameWithVariant)
                                .ToArray();
            var defaultIdx = 0;

            var window = ConfigWindow.Create(presetNames, defaultIdx, data);
            window.onApply.AddListener(OnApply);
        }

        /// <summary>
        /// 定数シンボル設定ツールの内部処理
        /// SymbolPresetSelectorWindowの適用ボタンが押されたときに呼び出される
        /// </summary>
        static void OnApply(ConfigWindow window, SymbolData.Preset preset, BuildTargetGroup[] buildTargets)
        {
            var result = EditorUtility.DisplayDialog(
                string.Format("定数プリセット {0} を設定しますか？", preset.name),
                string.Format("以下の定数が設定されます。\n\n{0}", preset.forScriptingDefineSymbols.Replace(';', '\n')),
                "OK",
                "キャンセル"
            );
            // 定数シンボルの変更が入ると再コンパイルが発生し、SymbolPresetSelectorWindowでエラーがでるので適用したら閉じる。
            if (result)
            {
                // 設定対象のビルドプラットフォームへ反映
                foreach (var buildTarget in buildTargets)
                {
                    preset.SetScriptingDefineSymbol(buildTarget);
                }

                window.Close();
            }
        }
    }

}