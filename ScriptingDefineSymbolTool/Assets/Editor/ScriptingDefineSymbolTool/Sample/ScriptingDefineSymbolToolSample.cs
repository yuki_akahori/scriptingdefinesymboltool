﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// ScriptingDefineSymbolToolの使用サンプルです
/// </summary>
static class ScriptingDefineSymbolToolSample
{
    const string _ConfigJSONPath = "Assets/Editor/ScriptingDefineSymbolTool/Sample/Data/config.json";

    [MenuItem("Tools/コンパイラ定数設定ウィンドウ起動")]
    static void BootConfigTool()
    {
        DefineSymbol.Util.BootConfigTool(_ConfigJSONPath);
    }

    [MenuItem("Tools/Defaultプリセットを適用")]
    static void SetPreset()
    {
        // ※variantsにBuildTargetGroupの名前を使用している前提での処理
        // iOS, Androidというvariantsがあれば、そのままBuildTargetGroupsにパースして処理を行える
        var symbolData = DefineSymbol.SymbolDataCreator.Create(_ConfigJSONPath);
        var presetGroup = symbolData.GetPresetGroup("Default");
        foreach (var preset in presetGroup.presets)
        {
            var buildTarget = (BuildTargetGroup)System.Enum.Parse(typeof(BuildTargetGroup), preset.variantName);
            preset.SetScriptingDefineSymbol(buildTarget);
            Debug.LogFormat("プリセット名 {0} をビルドターゲット {1} に設定しました", preset.nameWithVariant, preset.variantName);
        }

    }
}