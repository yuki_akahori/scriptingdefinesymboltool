﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace DefineSymbol
{
    /// <summary>
    /// 定数データ
    /// 定数シンボル情報と定数シンボルプリセットを持ちます
    /// </summary>
    public class SymbolData
    {
        /// <summary>
        /// 定数情報
        /// </summary>
        [System.Serializable]
        public class SymbolInfo
        {
            public SymbolInfo(string name, string comment)
            {
                this.name = name;
                this.comment = comment;
            }

            /// <summary>
            /// 定数名
            /// </summary>
            public string name;

            /// <summary>
            /// 説明表示用コメント
            /// </summary>
            public string commentForDisp { get { return comment ?? ""; } }  // コメントが設定されていることは保証されていないため

            /// <summary>
            /// 定数の説明コメント
            /// </summary>
            [SerializeField]
            string comment;
        }

        /// <summary>
        /// 定数プリセット
        /// </summary>
        public class Preset
        {
            /// <summary>
            /// コンストラクタ
            /// </summary>
            /// <param name="name">プリセット名</param>
            /// <param name="symbols">シンボル一覧</param>
            /// <param name="comment">概要コメント</param>
            /// <param name="variantName">バリアント名。ない場合はnullを設定してください。</param>
            public Preset(string name, string[] symbols, string comment, string variantName)
            {
                this.variantName = variantName == null ? "" : variantName;
                this.nameWithVariant = CombineVariants(name, variantName);
                this.name = name;
                this.symbols = symbols;
                this.comment = comment;
                forScriptingDefineSymbols = string.Join(";", symbols);
            }

            /// <summary>
            /// プリセット名
            /// </summary>
            public string name { get; private set; }

            /// <summary>
            /// バリアント込みのプリセット名
            /// </summary>
            public string nameWithVariant { get; private set; }

            /// <summary>
            /// プリセットで定義する定数
            /// </summary>
            public string[] symbols { get; private set; }

            /// <summary>
            /// 説明表示用コメント
            /// </summary>
            public string comment { get; private set; }

            /// <summary>
            /// UnityのScriptingDefineSymbolsにそのまま設定できるフォーマットでシンボルを取得します。
            /// </summary>
            public string forScriptingDefineSymbols { get; private set; }

            /// <summary>
            /// プリセットの対象プラットフォーム情報
            /// </summary>
            /// <value></value>
            public string platform { get; private set; }

            /// <summary>
            /// バリアント名
            /// </summary>
            public string variantName { get; private set; }

            /// <summary>
            /// ビルドターゲットに対して定数シンボルを設定する
            /// </summary>
            /// <param name="buildTargetGroup">設定対象のビルドターゲット</param>
            public void SetScriptingDefineSymbol(BuildTargetGroup buildTargetGroup)
            {
                PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, forScriptingDefineSymbols);
            }

            /// <summary>
            /// プリセット名にバリアント名をくっつけた名前取得します
            /// </summary>
            /// <param name="presetName">プリセット名</param>
            /// <param name="variantName">バリアント名</param>
            /// <returns>生成されたくっつけた文字</returns>
            public static string CombineVariants(string presetName, string variantName)
            {
                if (string.IsNullOrEmpty(variantName))
                {
                    // variantが存在する場合、別インスタンスを返すので、それに合わせて別インスタンスを返す
                    return string.Copy(presetName);
                }
                else
                {
                    return presetName + " - " + variantName;
                }
            }
        }

        /// <summary>
        /// プリセットグループ
        /// バリアント違いのプリセットを一つのグループとしてまとめています。
        /// </summary>
        public class PresetGroup
        {
            public PresetGroup(string presetName, Preset[] presets)
            {
                this.presetName = presetName;
                this.presets = presets;
            }

            public string presetName { get; private set; }
            public Preset[] presets { get; private set; }

            /// <summary>
            /// 指定したバリアント名のプリセットを取得します
            /// </summary>
            /// <param name="variantName">バリアント名</param>
            /// <returns>バリアント名に対応するプリセットがない場合nullを返します</returns>
            public Preset GetVariant(string variantName)
            {
                foreach (var preset in presets)
                {
                    if (preset.variantName == variantName)
                    {
                        return preset;
                    }
                }
                return null;
            }
        }

        public SymbolData(SymbolInfo[] symbolInfos, PresetGroup[] presetGroups)
        {
            _symbolInfoDict = new Dictionary<string, SymbolInfo>(capacity: symbolInfos.Length);
            foreach (var symbolInfo in symbolInfos)
            {
                _symbolInfoDict.Add(symbolInfo.name, symbolInfo);
            }

            _presetGroupDict = new Dictionary<string, PresetGroup>(capacity: presetGroups.Length);
            _presetDict = new Dictionary<string, Preset>();
            foreach (var presetGroup in presetGroups)
            {
                _presetGroupDict.Add(presetGroup.presetName, presetGroup);
                foreach (var preset in presetGroup.presets)
                {
                    _presetDict.Add(preset.nameWithVariant, preset);
                }
            }
        }

        /// <summary>
        /// プリセットの取得
        /// </summary>
        /// <param name="presetName">バリアント込のプリセット名</param>
        public Preset GetPreset(string presetNameWithVariant)
        {
            return _presetDict[presetNameWithVariant];
        }

        /// <summary>
        /// プリセットグループの取得
        /// </summary>
        /// <param name="presetGroupName">プリセットグループ名</param>
        public PresetGroup GetPresetGroup(string presetGroupName)
        {
            return _presetGroupDict[presetGroupName];
        }

        /// <summary>
        /// シンボル情報の取得
        /// </summary>
        /// <param name="symbolName">シンボル名</param>
        public SymbolInfo GetSymbolInfo(string symbolName)
        {
            if (!_symbolInfoDict.ContainsKey(symbolName))
            {
                // シンボル全てにコメントがつけられているとは限らない。
                // コメントデータが存在していなくても動作するようにする。
                // コメントが存在しない場合は空文字コメントを登録。
                _symbolInfoDict.Add(symbolName, new SymbolInfo(symbolName, ""));
            }

            return _symbolInfoDict[symbolName];
        }

        public IEnumerable<Preset> presets { get { return _presetDict.Values; } }

        Dictionary<string, SymbolInfo> _symbolInfoDict;
        Dictionary<string, Preset> _presetDict;
        Dictionary<string, PresetGroup> _presetGroupDict;
    }
}