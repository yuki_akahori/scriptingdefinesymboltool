﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace DefineSymbol
{
    /// <summary>
    /// 定数プリセット設定ウィンドウ
    /// ・プリセットの選択
    /// ・適用対象の選択
    /// を行い、適用ボタンを押すと設定情報を通知するウィンドウです
    /// ※定数設定処理は通知を受け取る側で実装してください
    /// </summary>
    public class ConfigWindow : EditorWindow
    {
        public static ConfigWindow Create(string[] presetNames, int defaultIdx, SymbolData symbolData)
        {
            var window = GetWindow<ConfigWindow>();
            window.titleContent.text = "定数プリセット";
            window.Initialize(presetNames, defaultIdx, symbolData);
            return window;
        }

        /// <summary>
        /// 適用ボタンが押されたときに発行されるイベントです
        /// </summary>
        public UnityEngine.Events.UnityEvent<ConfigWindow, SymbolData.Preset, BuildTargetGroup[]> onApply { get { return _onApply; } }

        enum BuildTargetMask
        {
            CurrentPlatform = 0,    // 現在エディタで設定しているビルドターゲットプラットフォーム
            iOS = 1 << 0,
            Android = 1 << 1,
            Standalone = 1 << 2,
            All = ~0
        }

        private void OnDestroy()
        {
            _onApply.RemoveAllListeners();
        }

        void Initialize(string[] presetNames, int defaultIdx, SymbolData symbolData)
        {
            _presetNames = presetNames;
            _symbolData = symbolData;
            if (_CurSelectIdx == -1)
            {
                _CurSelectIdx = defaultIdx;
            }
            _CurSelectIdx = Mathf.Clamp(_CurSelectIdx, 0, presetNames.Length - 1);
            UpdateSymbolInfo(_presetNames[_CurSelectIdx]);
        }

        void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            var prevIdx = _CurSelectIdx;

            _CurSelectIdx = EditorGUILayout.Popup("プリセット", _CurSelectIdx, _presetNames);
            if (prevIdx != _CurSelectIdx)
            {
                UpdateSymbolInfo(_presetNames[_CurSelectIdx]);
            }

            var preset = _symbolData.GetPreset(_presetNames[_CurSelectIdx]);
            if (!string.IsNullOrEmpty(preset.comment))
            {
                EditorGUILayout.LabelField("プリセット概要", preset.comment);
            }

            _CurSymbolInfoFold = EditorGUILayout.Foldout(_CurSymbolInfoFold, "定義される定数");
            if (_CurSymbolInfoFold)
            {
                EditorGUI.indentLevel++;
                foreach (var symbolInfo in _symbolInfos)
                {
                    EditorGUILayout.LabelField(symbolInfo.name, symbolInfo.commentForDisp);
                }
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.Space();

            _CurBuildTargetMask = (BuildTargetMask)EditorGUILayout.EnumFlagsField("適用対象", _CurBuildTargetMask);

            if (GUILayout.Button("適用"))
            {
                var presetName = _presetNames[_CurSelectIdx];

                List<BuildTargetGroup> targetGroups = new List<BuildTargetGroup>();
                if (_CurBuildTargetMask == BuildTargetMask.CurrentPlatform)
                {
                    targetGroups.Add(ToBuildTargetGroup(EditorUserBuildSettings.activeBuildTarget));
                }
                else
                {
                    if ((_CurBuildTargetMask & BuildTargetMask.Android) != 0)
                    {
                        targetGroups.Add(BuildTargetGroup.Android);
                    }
                    if ((_CurBuildTargetMask & BuildTargetMask.iOS) != 0)
                    {
                        targetGroups.Add(BuildTargetGroup.iOS);
                    }
                    if ((_CurBuildTargetMask & BuildTargetMask.Standalone) != 0)
                    {
                        targetGroups.Add(BuildTargetGroup.Standalone);
                    }
                }

                _onApply.Invoke(this, _symbolData.GetPreset(presetName), targetGroups.ToArray());
            }

            EditorGUILayout.EndVertical();
        }

        void UpdateSymbolInfo(string presetName)
        {
            var preset = _symbolData.GetPreset(presetName);
            _symbolInfos.Clear();
            foreach (var symbol in preset.symbols)
            {
                _symbolInfos.Add(_symbolData.GetSymbolInfo(symbol));
            }
        }

        public SymbolData data { get { return _symbolData; } }

        class OnApply : UnityEngine.Events.UnityEvent<ConfigWindow, SymbolData.Preset, BuildTargetGroup[]>
        {
        }

        static BuildTargetGroup ToBuildTargetGroup(BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.Android:
                    return BuildTargetGroup.Android;
                case BuildTarget.iOS:
                    return BuildTargetGroup.iOS;
                case BuildTarget.StandaloneLinux:
                case BuildTarget.StandaloneLinux64:
                case BuildTarget.StandaloneLinuxUniversal:
                case BuildTarget.StandaloneOSX:
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    return BuildTargetGroup.Standalone;
                case BuildTarget.WebGL:
                    return BuildTargetGroup.WebGL;
                default:
                    // 全部はカバーしていない。必要あれば追加
                    throw new System.Exception(string.Format("現在のビルドターゲット {0} に対応するBuildTargetGroupを設定してください", target));
            }
        }

        string[] _presetNames;
        static int _CurSelectIdx = -1;
        static bool _CurSymbolInfoFold = true;
        static BuildTargetMask _CurBuildTargetMask = BuildTargetMask.CurrentPlatform;
        List<SymbolData.SymbolInfo> _symbolInfos = new List<SymbolData.SymbolInfo>();
        SymbolData _symbolData;
        OnApply _onApply = new OnApply();
    }

}