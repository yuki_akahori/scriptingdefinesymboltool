# ScriptingDefineSymbolTool

## 機能・特徴
UnityのScripting Define Symbolsが設定を直接行うのは酷なので作成したツール。以下の機能・特徴を持つ。

- 定数のプリセットを作ることができ、プリセット単位でScripting Define Symbolsを設定することができる。
- 以下のようなプリセットの設定を行うウィンドウで設定を行うことができる  
![](./readmeImages/toolimage.png)
- バッチビルド時にも使えるようにしている。
    - 設定ウィンドウを経由しなくても定数の読み込み・設定が行える
    - JenkinsなどのCIツールで定数のプリセットを指定してビルドを行うような運用が行えるようにするため。
- 共通で設定したい定数をプリセットとは別に設定できる。
    - プロジェクトで絶対設定しておきたい定数がきっとあるはず。  
    そういったものをプリセットにもれなく記入するとミスが起きやすいため別に指定できるようにしている。
    - 指定のプリセットの定数を引き継ぐinclude機能を用意している。  
      共通のプリセットを作り、それをincludeすることで実現する。  
    - hiddenにtrueを設定すると設定ウィンドウからプリセットを隠すことができる。  
      includeでのみ使われ、実際にはユーザーに選択してほしくないプリセットにつけることを想定している。


```json
        {
            "presetGroups": [
                {
                    "name": "Common",
                    "symbols": [
                        "COMMON_SYMBOL"
                    ],
                    "hidden": true
                },
                {
                    "name": "includeSample",
                    "includePresets": [
                        "Common"
                    ],
                    "symbols": [
                        "HOGE"
                    ]
                }
            ]
        }
```  
      
- プリセットの変異を作ることができる。
    - プラットフォーム別に定数を設定したりする使い方を想定している。　　
    　以下ような記述を行うとVariantsSample - iOS, VariantsSample - Androidというプリセットが作れられる。

```json
    {
        "presetGroups": [
            {
                "name": "VariantsSample",
                "symbols": [
                    "COMMON_SYMBOL"
                ],
                "variants": [
                    {
                        "name": "iOS",
                        "symbols": [
                            "IOS_SYMBOL"
                        ]
                    },
                    {
                        "name": "Android",
                        "symbols": [
                            "ANDROID_SYMBOL"
                        ]
                    }
                ]
            }
        ]
    }
```

# 使い方

サンプルプロジェクトにサンプルデータを用意したのでそちらを見てください。  

サンプルデータのフォル: Assets/Editor/ScriptingDefineSymbolTool/Sample/Data/config.json
